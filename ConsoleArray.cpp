﻿#include <iostream>
#include <ctime>
#include <clocale>

using namespace std;
int const N = 10;

int main()
{
    int A[N][N];
    time_t Time = time(0);
    tm LocalTime;
    int IndexSum;
    int Sum = 0;

    setlocale(LC_CTYPE, "rus");
    localtime_s(&LocalTime, &Time);
    IndexSum = (&LocalTime)->tm_mday % N;

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            A[i][j] = i + j;
            cout << A[i][j] << " ";
        }
        cout << "\n";
    }

    cout << "Сумма элементов строки " << IndexSum << ": ";
    for (int i = 0; i < N; i++)
    {
        Sum += A[IndexSum][i];
    }
    cout << Sum << "\n";
}